import vertx

server = vertx.create_http_server()

@server.request_handler
def handle(req):
    req.response.end("Python rockin on")
    
server.listen(8083)
